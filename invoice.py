from jwcrypto import jwt, jws, jwk
from jwcrypto.common import json_encode
from datetime import datetime
import pickle
import uuid
from faker import Faker
import requests
import logging as logger
import asyncio

faker = Faker()


from models import InvoiceDto, TransactionItem

# hard coded key used
k={"k":'TWNRZlRqV25acjR1N3gheiVDKkYtSmFOZFJnVWtYcDI=',"kty":"oct"}

# method to sign the generated invoice
def signInvoice(dummyPayload):
    # generate key using jwk library
    # key = jwk.JWK.generate(kty="oct", size=256)
    key=jwk.JWK(**k)
    # exposeKey=key.export()
    # print(exposeKey)
    # claims to be tokenized
    Claims = {
        "iss": "auth_client_id",
        "sub": "business_code/business_id",
        "iat": str(datetime.now()),
        "jti": str(uuid.uuid4().hex),
        "invoice": dummyPayload
    }
    
   
    
    # sig = jwsToken.serialize()
    # generate a jwt token for the invoice
    token = jwt.JWT(header={"kid": "some_key_id", "alg": "HS256"}, claims=Claims)
    # signing the invoice 
    token.make_signed_token(key)
    # get the token serialized
    token.serialize()

    return token.serialize()
    





#  A method to generate invoice to be signed 
def generateInvoice():
    invoice = InvoiceDto()
    invoice.setTransactionId(str(uuid.uuid4().hex))
    invoice.setTernantId(str(uuid.uuid4().hex))
    invoice.setInvoiceDate(str(datetime.now()))
    invoice.setBuyerPhone("233544344980")
    invoice.setBuyerTin(str(faker.random_int()))
    invoice.setCashierName(str(faker.name()))
    invoice.setCashierTin(str(faker.random_int(min=5,max=10)))
    invoice.setDescription(faker.text())
    invoice.setGrossAmount(faker.random_int(min=8,max=12))
    invoice.setNetAmount(faker.random_int(min=8, max=12))
    invoice.setSellerName(faker.name())
    invoice.setSellerTin(str(faker.random_int(min=9,max=10)))
    invoice.setTotalDiscount(faker.random_int(min=8,max=12))
    invoice.setTotalTaxAmount(faker.random_int(min=8,max=10))
    
    # generate transactins
    ti1 = TransactionItem()
    ti1.setItemReference(str(uuid.uuid4().hex))
    ti1.setName(faker.name())
    ti1.setDescription(faker.text())
    ti1.setQuantity(faker.random_int(min=2,max=4))
    ti1.setUnitPrice(faker.random_int(min=4, max=10))
    ti1.setGrossAmount(ti1.getGrossAmount())
    ti1.setNetAmount(ti1.getNetAmount())
    ti1.setDiscountAmount(ti1.getDiscountedGrossAmount())
    ti1.setAppliedTax(faker.random_int(min=2, max=7))
   
    ti2 = TransactionItem()
    ti2.setItemReference(str(uuid.uuid4().hex))
    ti2.setName(faker.name())
    ti2.setDescription(faker.text())
    ti2.setQuantity(faker.random_int(min=2,max=4))
    ti2.setUnitPrice(faker.random_int(min=4, max=10))
    ti2.setGrossAmount(ti2.getGrossAmount())
    ti2.setNetAmount(ti2.getNetAmount())
    ti2.setDiscountAmount(ti2.getDiscountedGrossAmount())
    ti2.setAppliedTax(faker.random_int(min=2, max=7))


    invoice.setTransactionItems(ti1.toJason(),ti2.toJason())
    
    return invoice.toJason()
    

# a method to send the signed invoice
async def _sendInvoice(payload):

    url = 'http://34.120.138.14/staging/taxqrinvoicing/invoices/'

    _headers = {
        'X-Tenant-Id': 'adfsdfasdf',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwib3JnYW5pemF0aW9uIjoicG9zbW9iaWxlIiwiZXhwIjoyNjQyMzU2MzEzLCJhdXRob3JpdGllcyI6WyJGTV9BUFAiXSwianRpIjoiY2FlM2JkMWQtNTFkNC00MDJiLTkxYzQtMzg0ZmM0MmY2NDg1IiwiY2xpZW50X2lkIjoicG9zbW9iaWxlIn0.LQQXjh3fXjBCVfqQrxDoTI94aZiVkoqbFEWRKgsJsj7MQH2AKRcARLAZJ1CYC-DnKLKuGQTjqqNk8kRLxTA5x-kubzvqW1G-eSjnG5Zat4OaZOW2IwMLz6hkpST1rtd60eYC8SH-9NMDJ-ZmKuvf0oRgjkLix_808seFQhaDYDnarVNw6YS7Uk3UEhdcmSNxvPrqVAPwZGKJXpAY4-QEdEHyY9A01P6vYb5qmK33EtBaqpi33E_pmBABdy1ERWecaQ88jPdnHKddtUwmwqfR4qrdM2203YGiYtGvDFUJ6OkyyjWW1NfLDyRWQ8RoMFcLnJ9qTug7rUzDG6WzXdYu7g'
    }

    # converting the payload into a JSON format before sending 
    p = {
        "data": payload
    }

    re = requests.post(url=url, headers=_headers, json=p)
    # print(re.status_code)
    
    if (re.status_code == 201 or re.status_code == 200):
        # if the status code is 200 or 201, log the response
        logger.info(f' post request succesful  {re.status_code}')
        logger.info(f'{re.text}')
        # print(re.json())
        print('responds sent successfully')

        return print(f'{re.text}')
        
    else:
        # if an exception, raise the error 
        print('An error occured')
        raise  re.raise_for_status()
            


yet_to_send_Invoice = signInvoice(generateInvoice())

async def main():
    send = loop.create_task(_sendInvoice(yet_to_send_Invoice))
    await asyncio.wait([send])

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()  

