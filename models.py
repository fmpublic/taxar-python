from datetime import datetime
import uuid

class InvoiceDto:
    invoiceId = uuid.uuid4().hex
    ternantId = ''
    transactionId= ''
    buyerTin = ''
    buyerPhoneNumber = ''
    sellerTin = ''
    sellerName = ''
    cashierTin = ''
    cashierName = ''
    description=''
    grossAmount = 0.00
    netAmount = 0.00
    totalTaxAmount = 0.00
    totalDiscountAmount =0.00
    transactionItems = []
    invoiceDate = str(datetime.now()) 
    createdAt = str(datetime.now())
    updateAt = str(datetime.now())
    
    

    def setTransactionId(self, transactionId): 
        self.transactionId = transactionId

    def setInvoiceDate(self, instanceDate):
        self.invoiceDate = instanceDate

    def setBuyerPhone(self, number):
        self.buyerPhoneNumber = number

    def setBuyerTin(self, b_Tin):
        self.buyerTin = b_Tin

    def setCashierName(self, c_name):
        self.cashierName = c_name

    def setCashierTin(self, c_tin):
        self.cashierTin = c_tin

    def setNetAmount(self, net_amount):
        self.netAmount = net_amount

    def setSellerName(self, s_name):
        self.sellerName = s_name

    def setSellerTin(self, s_tin):
        self.sellerTin = s_tin

    def setTotalDiscount(self, total_disocunt):
        self.totalDiscountAmount = total_disocunt

    def setGrossAmount(self, gross_amount):
        self.grossAmount = gross_amount
        
    def setDescription(self, description):
        self.description = description
        
    def setTotalTaxAmount(self, total_tax_amount):
        self.totalTaxAmount = total_tax_amount

    def setTernantId(self, tenantId):
        self.ternantId = tenantId    

    def setTransactionItems(self, t1,t2 ):
        self.transactionItems.append(t1)
        self.transactionItems.append(t2)     

    def getGrossAmount(self):
        return self.grossAmount
    
    def clearTransactions(self):
        return self.transactionItems.clear()

    def toJason(self):
        return {
            'id': self.invoiceId,
            'tenantId': self.ternantId,
            'transactionId': self.transactionId,
            'description': self.description,
            'buyerPhone': self.buyerPhoneNumber,
            'buyerTin': self.buyerTin,
            'cashierName': self.cashierName,
            'cashierTin': self.cashierTin,
            'sellerName': self.sellerName,
            'sellerTin': self.sellerTin,
            'grossAmount': self.grossAmount,
            'netAmount': self.netAmount,
            'totalDiscountAmount': self.totalDiscountAmount,
            'totalTaxAmount': self.totalTaxAmount,
            'transactionItems': self.transactionItems,
            'invoiceDate': self.invoiceDate,
            'createdAt': self.createdAt,
            'updatedAt': self.updateAt,
        } 




class TransactionItem:
    itemRefrence = ''
    itemName = ''
    description = ''
    quantity = 0
    unitPrice = 0.00
    netAmount = 0.00
    discountAmount = 0.00
    appliedTax = 0.00
    grossAmount = 0.00

    def setItemReference(self, item_reference):
        self.itemRefrence = item_reference

    def setName(self, itemName):
        self.itemName = itemName

    def setDescription(self, description):
        self.description = description

    def setQuantity(self, quantity):
        self.quantity = quantity

    def setUnitPrice(self, u_price):
        self.unitPrice = u_price

    def setGrossAmount(self, g_amount):
        pass

    def setNetAmount(self, net_amount):
        self.netAmount = net_amount

    def setDiscountAmount(self,discount_amount):
        self.discountAmount = discount_amount

    def setAppliedTax(self, applied_tax):
        self.appliedTax = applied_tax

    def getGrossAmount(self):
        return (self.unitPrice * self.quantity)

    def getTaxAmount(self):
        return 20.8    

    def getDiscountedGrossAmount(self):
        return (self.grossAmount - self.discountAmount)

    def getNetAmount(self):
        return (self.getDiscountedGrossAmount() + self.getTaxAmount())           
    
    def toJason(self):
      return {
            'itemReference':self.itemRefrence,
            'name': self.itemName,
            'description': self.description,
            'quantity': self.quantity,
            'unitPrice': self.unitPrice,
            'grossAmount': self.grossAmount,
            'netAmount': self.netAmount,
            'discountAmount': self.discountAmount,
            'appliedTax': self.appliedTax,
      }



