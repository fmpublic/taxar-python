import setuptools

VERSION = "0.0.1"

setuptools.setup(
    name="taxQR-tax-calculator-sdk-finance-mobile",
    version=VERSION,
    author="Finance Mobile"
    description="An sdk to help calculate various tax",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programing Language :: python :: 3",
        "License :: OSI aproved :: MIT License",
    ],
    python_requires= '>=3.6',
    )