from os import path
import json
import jsonschema
from jsonschema import validate
import logging as log



# log basic config
# log.basicConfig(filename = "taxqrlogs.log", encoding='utf-8', level=["log.DEBUG,log.INFO"])
# set config data
configSchema = {
     'type': 'object',
     'properties': {
         'clientKey': {'type': 'string'},
         'encryptionKey':{'type':'string'},
     },
 }

# check if config path exist
def configExists(configPath):
    return path.exists(configPath)

# validate the config file 
def validateJson(jsonData):
    try:
        # validate the jsonData
        validate(instance=jsonData,schema=configSchema)
    except jsonschema.exceptions.ValidationError as err:
        # log this error if instance is not correct
        log.error(f'{err}')
        return False
    return True

# a method to read the config file 
def readConfig(configPath):
    # read the content from the config file 
    with open(configPath, 'r') as configFile:
        # read the file into the content
        content = configFile.read()
        # convert the json into python dictionary
        obj = json.load(content)
        # return python dict
    return obj    

#the init method which contains the main functionlity of the sdk
def init(configPath):
    # check if config file path is valid 
    try:
        configFileExist = configExists(configPath)
        return configFileExist
    except IOError as err:
        return log.error(f'{err}')
    
    if configFileExist:
        # read the config file 
        content = readConfig(configPath)
        try:
             # check if the json content file is valid
            isValid = validateJson(content)
            log.info(f'was able to read the config file')
            
            # TODO: create a database
            # return success message of how the payload looks like
        except Exception as err:
            log.error(f'{err}')
            return err
        return isValid
    else:
        log.error(f'was not able to read the config file')

    return True    
        
