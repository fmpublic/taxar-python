import enum
from urllib.error import HTTPError, URLError
import urllib
import logging as log
import requests
from jwcrypto import jwk, jws,jwe
from jwcrypto.common import json_encode

# loggin config
# log.basicConfig(filename = "taxqrlogs.log", encoding='utf-8', level=1)


class CONNECTION(enum.Enum):
    CONNECTED= 1
    DISCONNECTED=2

# method to check for client's internet connection 
def checkInternet():
    url = "www.google.com"
    timeout = 5
    try:
        request = requests.get(url, timeout)
             
        log.info('connection successful:')
        log.info(f'{request}')
        return CONNECTION.CONNECTED
    except (requests.ConnectionError, requests.Timeout) as exceptions:
        log.error('something went wrong:')
        log.error(f'{exceptions}')
        
        return CONNECTION.DISCONNECTED



async def sign(invoice_id,key):
    # TODO: get the invoice with this id from the database

    # generate jwt string 
    payload = "hello taxar"
    key = jwk.JWK.generate(kty="oct", size=256)
    jwsToken = jws.JWS(payload.encode('utf-8'))
    jwsToken.add_signature(key,None,json_encode({"alg":"HS256"}), json_encode({"kid":"key.thumbprint"}))
    jwtSign = jwsToken.serialize()
        
    print(key) 
    print(jwtSign)
    print(jwsToken)
    return jwtSign; 
    # CHECK IF CONNECTED
    # connection = await checkInternet()

    # if connection==CONNECTION.CONNECTED:
    #     #TODO: send the inverse to the server